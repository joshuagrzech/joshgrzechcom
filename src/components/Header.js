import React from 'react'

const Header = () => {
  
    return (
       
        <header className="header light">
            <div className="sticky">
                <div className="container">
                    <div className="logo"> <a href="index.html"><img src="images/logo.png" alt="" /></a> </div>
                    <nav className="navbar ownmenu">
                        <div className="navbar-header">
                            <button type="button" className="navbar-toggle collapsed" data-toggle="collapse" data-target="#nav-open-btn" aria-expanded="false"> <span><i className="fa fa-navicon"></i></span> </button>
                        </div>
                        <div className="collapse navbar-collapse" id="nav-open-btn">
                            <ul className="nav">
                                <li className="dropdown active"> <a href="index.html" className="dropdown-toggle" data-toggle="dropdown">HOME </a>
                                    <ul className="dropdown-menu">
                                        <li> <a href="index.html">HOME Defult</a> </li>
                                        <li> <a href="index-1.html">HOME Personal</a> </li>
                                        <li> <a href="index-onepage.html">HOME one page layout <span>( New )</span></a> </li>
                                        <li> <a href="index-2.html">HOME Menu 2 </a> </li>
                                        <li> <a href="index-boxed.html">HOME Boxed </a> </li>
                                        <li> <a href="index-video.html">HOME Video Background <span>( New )</span></a> </li>
                                        <li> <a href="index_mp-3b.html">HOME Portfolio <span>( Creative )</span></a> </li>
                                        <li> <a href="index-slider.html">HOME Slider <span>( New )</span></a> </li>
                                        <li> <a href="index-flat-text.html">HOME Flat Text</a> </li>
                                    </ul>
                                </li>
                                <li className="dropdown"> <a href="#." className="dropdown-toggle" data-toggle="dropdown">Pages</a>
                                    <ul className="dropdown-menu">
                                        <li> <a href="services-1.html">Services 01</a> </li>
                                        <li> <a href="services-2.html">Services 02</a> </li>
                                        <li> <a href="services-3.html">Services 03</a> </li>
                                        <li> <a href="index_mp-3a.html">Portfolio 01</a> </li>
                                        <li> <a href="index_mp-3b.html">Portfolio 02</a> </li>
                                        <li> <a href="index_mp-3c.html">Portfolio 03</a> </li>
                                        <li> <a href="index_mp-3d.html">Portfolio 04</a> </li>
                                        <li> <a href="portfolio_detail_01.html">portfolio detail 01 </a> </li>
                                        <li> <a href="portfolio_detail_02.html">portfolio detail 02 </a> </li>
                                        <li> <a href="portfolio_detail_03.html">portfolio detail 03 </a> </li>
                                        <li> <a href="blog.html">blog </a> </li>
                                        <li> <a href="blog-list-page-1.html">blog list page 01 </a> </li>
                                        <li> <a href="blog-list-page-2.html">blog  list page 02</a> </li>
                                        <li> <a href="blog-details.html">Blog Details</a> </li>
                                        <li> <a href="contact.html">contact </a> </li>
                                        <li> <a href="coming-soon-1.html">COMING SOON 1 </a> </li>
                                        <li> <a href="coming-soon-2.html">COMING SOON 2</a> </li>
                                        <li> <a href="coming-soon-3.html">COMING SOON 3</a> </li>
                                    </ul>
                                </li>
                                <li> <a href="about.html">ABOUT </a> </li>
                                <li className="dropdown"> <a href="services-1.html" className="dropdown-toggle" data-toggle="dropdown">SERVICES</a>
                                    <ul className="dropdown-menu">
                                        <li> <a href="services-1.html">Services 01</a> </li>
                                        <li> <a href="services-2.html">Services 02</a> </li>
                                        <li> <a href="services-3.html">Services 03</a> </li>
                                    </ul>
                                </li>
                                <li className="dropdown"> <a href="index_mp-3a.html" className="dropdown-toggle" data-toggle="dropdown">PORTFOLIO</a>
                                    <ul className="dropdown-menu">
                                        <li> <a href="index_mp-3a.html">Portfolio 01</a> </li>
                                        <li> <a href="index_mp-3b.html">Portfolio 02</a> </li>
                                        <li> <a href="index_mp-3c.html">Portfolio 03</a> </li>
                                        <li> <a href="index_mp-3d.html">Portfolio 04</a> </li>
                                        <li> <a href="portfolio_detail_01.html">portfolio detail 01 </a> </li>
                                        <li> <a href="portfolio_detail_02.html">portfolio detail 02 </a> </li>
                                        <li> <a href="portfolio_detail_03.html">portfolio detail 03 </a> </li>
                                    </ul>
                                </li>
                                <li className="dropdown"> <a href="blog.html" className="dropdown-toggle" data-toggle="dropdown">blog </a>
                                    <ul className="dropdown-menu">
                                        <li> <a href="blog.html">blog </a> </li>
                                        <li> <a href="blog-list-page-1.html">blog list page 01 </a> </li>
                                        <li> <a href="blog-list-page-2.html">blog  list page 02</a> </li>
                                        <li> <a href="blog-details.html">Blog Details</a> </li>
                                    </ul>
                                </li>
                                <li> <a href="contact.html">CONTACT</a> </li>
                            </ul>
                        </div>
                    </nav>
                </div>
            </div>
        </header>
       
    )
}

export default Header