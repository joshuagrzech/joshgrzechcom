import React from 'react'

const Portfolio = () => {
    return (
        <section class="our-work padding-top-150 padding-bottom-150" data-stellar-background-ratio="0.4" >
            <div class="container">

                <div class="heading-block">
                    <h6 class="animate fadeInRight" data-wow-delay="0.4s">awesome projects</h6>
                    <span class="huge-tittle">my work</span> </div>

                <div class="list-work">

                    <article>
                        <ul class="row">

                            <li class="col-md-5">
                                <div class="text-info text-right">
                                    <h3 class="tittle">Specs design that everyone’s suits hairstyle</h3>
                                    <p>Lorem ipsdolor sit amet, consectetur adipisc elit. Donec quis erat sed elit sceis Lorem ipsum dolor sit amet, </p>
                                    <div class="text-center"> <a href="#." class="btn-flat">live project</a> </div>
                                </div>
                            </li>


                            <li class="col-md-7"> <img class="img-right animate fadeInRight" data-wow-delay="0.4s" src="images/portfolio/img-1-1.jpg" alt="" /> </li>
                        </ul>
                    </article>


                    <article>
                        <ul class="row">

                            <li class="col-md-7"> <img class="animate fadeInLeft" data-wow-delay="0.4s" src="images/portfolio/img-1-2.jpg" alt="" /> </li>

                            <li class="col-md-5">
                                <div class="text-info text-left">
                                    <h3 class="tittle">Specs design that everyone’s suits hairstyle</h3>
                                    <p>Lorem ipsdolor sit amet, consectetur adipisc elit. Donec quis erat sed elit sceis Lorem ipsum dolor sit amet, </p>
                                    <div class="text-center"> <a href="#." class="btn-flat">live project</a> </div>
                                </div>
                            </li>
                        </ul>
                    </article>

                    <article>
                        <ul class="row">


                            <li class="col-md-5">
                                <div class="text-info text-right">
                                    <h3 class="tittle">Specs design that everyone’s suits hairstyle</h3>
                                    <p>Lorem ipsdolor sit amet, consectetur adipisc elit. Donec quis erat sed elit sceis Lorem ipsum dolor sit amet, </p>
                                    <div class="text-center"> <a href="#." class="btn-flat">live project</a> </div>
                                </div>
                            </li>


                            <li class="col-md-7"> <img class="img-right animate fadeInRight" data-wow-delay="0.4s" src="images/portfolio/img-1-3.jpg" alt="" /> </li>
                        </ul>
                    </article>

                    <article>
                        <ul class="row">


                            <li class="col-md-7"> <img class="animate fadeInLeft" data-wow-delay="0.4s" src="images/portfolio/img-1-4.jpg" alt="" /> </li>


                            <li class="col-md-5">
                                <div class="text-info text-left">
                                    <h3 class="tittle">Specs design that everyone’s suits hairstyle</h3>
                                    <p>Lorem ipsdolor sit amet, consectetur adipisc elit. Donec quis erat sed elit sceis Lorem ipsum dolor sit amet, </p>
                                    <div class="text-center"> <a href="#." class="btn-flat">live project</a> </div>
                                </div>
                            </li>
                        </ul>
                    </article>


                    <div class="text-center"> <a href="#." class="btn-large">load more work</a> </div>
                </div>
            </div>
        </section>
    )
}

export default Portfolio