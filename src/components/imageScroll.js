
import React, { useEffect } from 'react';
import { withScroll } from 'react-fns';

const html = document.documentElement;
export const ImageScroll = (props) => {
    const [loaded, setLoaded] = React.useState(false)
    const [height] = React.useState(window.innerHeight)
    
   
    const canvasRef = React.createRef()

    const currentFrame = index => {
        return (`${props.source}${Math.abs(index - props.startPosition)}.png`)
    }
    const currentBgFrame = index => {
        return (`${props.source}bg_${Math.abs(index - props.startPosition)}.png`)
    }
    const img = new Image()
    const bgImg = new Image()
    img.src = currentFrame(1);
    bgImg.src = currentBgFrame(0)
    const frameCount = props.frameCount;
    useEffect(() => {

        if (loaded === false) {
            let i = 0
            const preloadImages = () => {
                for (i; i < frameCount; i++) {
                    const img = new Image();
                    img.src = currentFrame(i);
                }
            };
            preloadImages()
            if (i > (frameCount - 100)) {
                setLoaded(true)
            }
        }
        const context = canvasRef.current.getContext("2d");
        canvasRef.current.width = props.width
        canvasRef.current.height = props.height;

        img.onload = function () {
            if (props.transparent === true) {
                context.clearRect(0, 0, window.innerWidth, height)
                context.drawImage(img, 0, 0, img.width, img.height,
                    0, 0, props.width, props.height);
            } else {
                context.drawImage(img, 0, 0, img.width, img.height,
                    0, 0, props.width, props.height);
            }

        }
        const updateImage = index => {
            console.log(index + img.src)
            img.src = currentFrame(index);
            if (index >= props.startPosition) {
                if (props.transparent === true) {

                    context.drawImage(img, 0, 0, img.width, img.height,
                        0, 0, props.width, props.height);
                } else {
                    context.drawImage(img, 0, 0, img.width, img.height,
                        0, 0, props.width, props.height);
                }
            }
        }
        window.addEventListener('scroll', () => {
            const scrollTop = window.scrollY;
            const maxScrollTop = props.length;
            const scrollFraction = scrollTop / maxScrollTop;
            const frameIndex = Math.min(
                frameCount - 1,
                Math.ceil(scrollFraction * frameCount)
            );
            requestAnimationFrame(() => {


                updateImage((frameIndex + 1))

            
                        
            
                })
    });
    setTimeout(() => {
        setDone(true)
    }, 5000)


}, [])



return (
    <>
        <div style={{ width: '100%', backgroundColor: 'white' }} >
            <canvas ref={canvasRef} id="hero-lightpass" style={{
                position: window.scrollY > props.length ? 'absolute' : props.canvasPosition,
                top: props.topPosition,
                left: props.leftPosition
            }} />
        </div>



    </>
);
}
export default withScroll(ImageScroll)
