import React from 'react'
import Lottie from 'lottie-react';
import Loading from './528-spinner-loading.json'
import styled, { keyframes } from "styled-components";
import BounceInUp from 'react-animations/lib/bounce-in-up'
const Loader = () => {
    const BounceInUpAnimation = keyframes`${BounceInUp}`;
    const BounceInUpDiv = styled.div`
      animation: 1s ${BounceInUpAnimation};
      
    `;
    return (
       
            <BounceInUpDiv style={{ backgroundColor: 'white', height: '100%', width: '100%', position: 'relative', paddingBottom: window.innerHeight }}>
                <Lottie animationData={Loading} height={window.innerHeight} width={window.innerWidth} style={{ height: '50%', width: '50vw', marginLeft: '25%', marginBottom: '15%', position: 'relative' }} options={{ autoPlay: true, loop: true }} />
            </BounceInUpDiv>
      
    )
}

export default Loader