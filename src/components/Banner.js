import React from 'react'
import styled, { keyframes } from "styled-components";
import FlipInY from 'react-animations/lib/flip-in-y'

const Banner = () => {
 
    const FlipInAnimation = keyframes`${FlipInY}`;
    const FlipInDiv = styled.div`
  animation: 2s ${FlipInAnimation};
  aniamtion-delay: 10s;
  `;
    return (
        <>
            
            <div className="home-agency" style={{paddingTop: '0%'}}>
                <div className="position-center-center">
                    <div className="container">
                        <FlipInDiv>
                            <div className="ag-text">

                                <h3 className="animate fadeInDown" data-wow-delay="0.4s">JOSH</h3>
                                <h1 className="animate fadeInDown" data-wow-delay="0.6s">GRZECH</h1>

                                <p className="animate fadeInDown" data-wow-delay="0.8s">UI/UX Designer & Back End Engineer</p>
                            </div>
                        </FlipInDiv>
                    </div>
                </div>
            </div>
        </>
    )
}

export default Banner
