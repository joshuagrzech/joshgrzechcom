import React from 'react'
import ImageScroll from './imageScroll'
import { withScroll } from 'react-fns';
const Intro = () => {
    const inView = () => {
        if ((window.pageYOffset >= window.innerHeight + 300) && (window.pageYOffset < window.innerHeight * 4)) {
            return true
        } else {
            return false
        }
    }
    const scrollDone = () => {
        if (window.pageYOffset > window.innerHeight + 300) {
            return true
        } else {
            return false
        }
    }
    return (
    <>
        
        <section class="hello" style={{width: '85%', left: '7%', paddingBottom: window.innerHeight * 4, position: 'relative', zIndex: 1}}>
            
            <div class="cir-tri-bg" />


            <div class="container">
                <div class="heading-block">
                    <h6 class="animate fadeInRight" data-wow-delay="0.4s">about me</h6>
                    <span class="huge-tittle">junior developer</span>
                </div>
                <div class="col-sm-5">
                    
                </div>
                <div class="col-sm-7" style={{position: 'fixed', zIndex: 2 }}>
                    <h4>I'm a UX/UI designer Austin based, who loves clean, simple & unique design. I also enjoy crafting brand identities, icons, & illustration work.</h4>
                    <div class="test-info">
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec quis erat sed elit sceleris Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec quis erat sed elit scelerisque iaculis. Nullam nec porttitor justo, at iaculis risus. Aliquam erat volutpat. que iaculis.</p>
                        <a href="#." class="btn-flat">checkout my profile</a>
                    </div>
                </div>
                <ImageScroll
      source={`/images/scrollscenes/2/`}
      frameCount={279}
      startPosition={0}
      length={window.innerHeight * 8}
      width={window.innerWidth / 3}
      height={window.innerHeight}
      canvasPosition={inView() === true ? 'fixed' : 'relative'}
      topPosition={100}
      leftPosition={200}
      transparent={true}
  />
            </div>
            

            <div class="cir-bottom-bg" />
            
            
        </section>
      
  {scrollDone() === true ? (
            <div style={{ }}></div>
        ) : null}
  </>
    )
}

export default withScroll(Intro)