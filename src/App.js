import React, { useEffect } from 'react'
import './App.css';
import Loader from './components/Loader';
import Header from './components/Header';
import styled, { keyframes } from "styled-components";
import FadeIn from 'react-animations/lib/fade-in'
import Banner from './components/Banner';
import Intro from './components/Intro';
import ImageScroll from './components/imageScroll'
import Portfolio from './components/Portfolio';

const App = () => {

  const [done, setDone] = React.useState(false)
  useEffect(() => {
    if (done === false) {
      setTimeout(() => {
        setDone(true)
      }, 500)
    }

  }, [done])
  const FadeInAnimation = keyframes`${FadeIn}`;
  const FadeInDiv = styled.div`
  animation: 2s ${FadeInAnimation};
  
  `;
  return (
    <>
      {done === false ? (
        <Loader />
      ) : (
        <>
        <ImageScroll
                source={`/images/scrollscenes/1/`}
                
                frameCount={500}
                startPosition={0}
                length={window.innerHeight * 6}
                width={window.innerWidth}
                height={window.innerHeight}
                canvasPosition={'fixed'}
                
            />
            
                    
        <FadeInDiv>
          
          <div className="wrap" style={{zIndex: 'auto'}}>
            <Header />
            <Banner />
            <div id="content"> 
            <Intro/>
            
            <Portfolio/>
            </div>
          </div>
        </FadeInDiv>
        </>
      )}
    </>


  );
}

export default App;
